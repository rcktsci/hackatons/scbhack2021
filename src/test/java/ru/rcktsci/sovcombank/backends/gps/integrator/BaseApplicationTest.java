package ru.rcktsci.sovcombank.backends.gps.integrator;

import com.gitlab.simsonic.scbhack.ScbHackApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(
        classes = ScbHackApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@Slf4j
public class BaseApplicationTest {

    @Test
    public void contextIsCorrect() {
        log.info("I'm fine.");
    }
}
