package com.gitlab.simsonic.scbhack.server.service;

import com.gitlab.simsonic.scbhack.server.api.GetUserResponse;
import com.gitlab.simsonic.scbhack.server.service.details.UserDetailsSoapService;
import com.gitlab.simsonic.scbhack.server.service.phones.UserPhonesResponseDto;
import com.gitlab.simsonic.scbhack.server.service.phones.UserPhonesRestService;
import com.gitlab.simsonic.scbhack.server.utils.AsyncUtils;
import com.gitlab.simsonic.scbhack.server.utils.AsyncUtils.Wrapper;
import com.gitlab.simsonic.scbhack.soap.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
public class ScbHackUiService {

    private final UserDetailsSoapService userDetailsSoapService;
    private final UserPhonesRestService userPhonesRestService;

    public GetUserResponse getUser(int userId) {
        Future<User> soapFuture = userDetailsSoapService.getUserDetailsAsync(userId);
        Future<UserPhonesResponseDto> restFuture = userPhonesRestService.getUserPhones(userId);

        Wrapper<User> user = AsyncUtils.awaitAsync("SOAP", soapFuture);
        Wrapper<UserPhonesResponseDto> phone = AsyncUtils.awaitAsync("REST", restFuture);

        switch (user.getWaitResult()) {
            case OK:
                return build(user.getValue(), extractPhone(phone));
            case TIMED:
                return GetUserResponse.TIMEOUT;
            case ERROR:
                return GetUserResponse.ERROR;
        }

        throw new IllegalStateException("wtf?");
    }

    private static String extractPhone(Wrapper<UserPhonesResponseDto> phone) {
        if (phone.getWaitResult() == AsyncUtils.WaitResult.OK) {
            return StreamEx.of(phone.getValue())
                    .flatCollection(UserPhonesResponseDto::getPhones)
                    .findFirst(StringUtils::isNotBlank)
                    .orElse(null);
        }
        return null;
    }

    private static GetUserResponse build(User user, String phone) {
        String name = StreamEx.of(user.getFirstName(), user.getLastName())
                .filter(StringUtils::isNotBlank)
                .joining(" ");
        return GetUserResponse.ok(name, phone);
    }
}
