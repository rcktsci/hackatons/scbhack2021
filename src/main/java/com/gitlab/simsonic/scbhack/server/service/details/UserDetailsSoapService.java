package com.gitlab.simsonic.scbhack.server.service.details;

import com.gitlab.simsonic.scbhack.soap.GetUserRequest;
import com.gitlab.simsonic.scbhack.soap.GetUserResponse;
import com.gitlab.simsonic.scbhack.soap.SoapServicePort;
import com.gitlab.simsonic.scbhack.soap.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsSoapService {

    private final SoapServicePort soapServicePort;

    @Async
    public Future<User> getUserDetailsAsync(int userId) {
        User result = getUserDetails(userId);
        return AsyncResult.forValue(result);
    }

    private User getUserDetails(int userId) {
        GetUserRequest request = new GetUserRequest();
        request.setUserId(userId);
        GetUserResponse response = soapServicePort.getUser(request);
        return response.getUser();
    }
}
