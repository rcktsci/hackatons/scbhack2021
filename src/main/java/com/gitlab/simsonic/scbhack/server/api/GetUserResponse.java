package com.gitlab.simsonic.scbhack.server.api;

import lombok.Value;

@Value
public class GetUserResponse {

    private static final int OK_CODE = 0;
    private static final int TIMEOUT_CODE = 1;
    private static final int ERROR_CODE = 2;

    public static final GetUserResponse TIMEOUT = new GetUserResponse(TIMEOUT_CODE, null, null);
    public static final GetUserResponse ERROR = new GetUserResponse(ERROR_CODE, null, null);

    int code;
    String name;
    String phone;

    public static GetUserResponse ok(String name, String phone) {
        return new GetUserResponse(OK_CODE, name, phone);
    }
}
