package com.gitlab.simsonic.scbhack.server.utils;

import lombok.Value;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@UtilityClass
@Slf4j
public class AsyncUtils {

    public static <T> Wrapper<T> awaitAsync(
            String serviceName,
            Future<T> future
    ) {
        try {
            T result = future.get(5, TimeUnit.SECONDS);
            return new Wrapper<>(WaitResult.OK, result);
        } catch (InterruptedException | ExecutionException ignored) {
            return new Wrapper<>(WaitResult.ERROR, null);
        } catch (TimeoutException ex) {
            log.warn("Timeout reached, skipping service: {}", serviceName);
            return new Wrapper<>(WaitResult.TIMED, null);
        }
    }

    public enum WaitResult {

        OK,
        TIMED,
        ERROR,
        ;
    }

    @Value
    public static class Wrapper<T> {

        WaitResult waitResult;
        T value;
    }
}
