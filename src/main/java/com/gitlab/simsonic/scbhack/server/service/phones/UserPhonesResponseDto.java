package com.gitlab.simsonic.scbhack.server.service.phones;

import lombok.Value;

import java.util.List;

@Value
public class UserPhonesResponseDto {

    List<String> phones;
}
