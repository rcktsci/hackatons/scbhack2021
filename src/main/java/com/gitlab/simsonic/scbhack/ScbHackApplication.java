package com.gitlab.simsonic.scbhack;

import com.gitlab.simsonic.scbhack.config.ScbHackProperties;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(value = ScbHackProperties.class)
@SpringBootApplication
public class ScbHackApplication {

    public static void main(String[] args) {
        //noinspection resource
        new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(ScbHackApplication.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }
}
