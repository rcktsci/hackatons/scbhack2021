package com.gitlab.simsonic.scbhack.config;

import com.gitlab.simsonic.scbhack.soap.ObjectFactory;
import com.gitlab.simsonic.scbhack.soap.SoapServicePort;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

@Configuration
@Slf4j
public class SoapConfig {

    @Bean
    public FactoryBean<SoapServicePort> soapClient(ScbHackProperties scbHackProperties) {
        return createFactoryBean(
                "soapService.wsdl",
                "SoapServicePortService",
                scbHackProperties.getWsEndpoint(),
                SoapServicePort.class);
    }

    @SneakyThrows
    private static <T> FactoryBean<T> createFactoryBean(
            String classPathResource,
            String serviceName,
            String endpointAddress,
            Class<? extends T> serviceInterface
    ) {
        JaxWsPortProxyFactoryBean factoryBean = new JaxWsPortProxyFactoryBean();

        Resource wsdlResource = new ClassPathResource(classPathResource);
        factoryBean.setWsdlDocumentResource(wsdlResource);
        factoryBean.setServiceName(serviceName);
        factoryBean.setEndpointAddress(endpointAddress);

        log.info("Setting URL for interface {}: {}", serviceInterface.getSimpleName(), endpointAddress);
        factoryBean.setServiceInterface(serviceInterface);

        //noinspection unchecked
        return (FactoryBean<T>) factoryBean;
    }

    @Bean
    public ObjectFactory objectFactory() {
        return new ObjectFactory();
    }
}
